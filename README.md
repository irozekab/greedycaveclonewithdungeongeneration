# GreedyCaveCloneWithDungeonGeneration

### What is this repository for?

GreedyCaveCloneWithDungeonGeneration is my first attempt in dungeon generation. It contains a generator that will generate a maze like dungeon with rooms.

Pathing of characters are done via A* pathfinding.

### What is the current state?

No Kameosa library exists. Alot of refactoring required.

### How do I get set up?

1. Download Unity and run Unity.
2. Update project to latest Unity version if necessary.
3. Fix compilation errors which might show up in future Unity version.
4. Start coding.