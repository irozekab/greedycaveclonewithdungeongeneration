﻿using UnityEngine;

public class RandomizeSprite : MonoBehaviour
{
	public SpriteRenderer spriteRenderer;
	public Sprite[] sprites;
	public bool isRandomize = true;

	void Awake ()
	{
	}

	void Start ()
	{
		if (this.isRandomize && this.sprites.Length > 1)
		{
			int i = Random.Range(0, this.sprites.Length);
			this.spriteRenderer.sprite = this.sprites[i];
		}
	}
	
	void Update()
	{
	}
}
