﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;
using Kameosa;
using DungeonGeneration;

public class PlayerController : MonoBehaviour
{
    private MobileController mobileController;

    void Start()
    {
        this.mobileController = GetComponent<MobileController>();
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(Constants.LEFT_MOUSECLICK))
        {
            if (!this.mobileController.isMoving)
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction);

                if (hit)
                {
                    Node destination = hit.transform.GetComponent<NodeController>().node;
                    Stack<Node> path = Dungeon.GetPath(this.mobileController.node, destination);

                    this.mobileController.Move(path);
                }
            }
            else if (this.mobileController.isMoving)
            {
                this.mobileController.StopMoving();
            }
        }

        Camera.main.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, Camera.main.transform.position.z);
    }
}