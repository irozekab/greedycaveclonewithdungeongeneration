﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;
using Kameosa;
using DungeonGeneration;

public class MobileController : MonoBehaviour
{
    public Animator animator;
    public Node node;
	public float moveTime = 10f;

	[System.NonSerialized]
    public bool isMoving = false;

	private Rigidbody2D rigidBody;
    private Coroutine currentMovingCoroutine;

    void Start()
    {
		this.rigidBody = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
    }

	public void Move(Stack<Node> path)
	{
		if (!this.isMoving)
		{
			this.isMoving = true;

			this.currentMovingCoroutine = StartCoroutine(MoveSmoothly(path));
		}
	}

	public void Move(Node step)
	{
		if (!this.isMoving)
		{
			this.isMoving = true;

			this.currentMovingCoroutine = StartCoroutine(MoveSmoothly(step));
		}
	}

	public void StopMoving()
	{
        this.isMoving = false;
        this.animator.SetBool("isMoving", false);

        StopCoroutine(this.currentMovingCoroutine);
	}

	IEnumerator MoveSmoothly(Stack<Node> path)
	{
        while (path.Count > 0)
        {
			Node currentStep = path.Pop();
            Coordinate direction = currentStep.Coordinate - this.node.Coordinate;

            this.animator.SetBool("isMoving", true);
            this.animator.SetFloat("xVelocity", direction.X);
            this.animator.SetFloat("yVelocity", direction.Y);

            float sqrRemainingDistance = (this.transform.position - currentStep.Coordinate).sqrMagnitude;

            while (sqrRemainingDistance > float.Epsilon)
            {
                Vector3 newPosition = Vector3.MoveTowards(this.transform.position, currentStep.Coordinate, this.moveTime * Time.deltaTime);
                rigidBody.MovePosition(newPosition);
                sqrRemainingDistance = (this.transform.position - currentStep.Coordinate).sqrMagnitude;

                yield return null;
            }

			this.node = currentStep;
        }

		this.isMoving = false;
		this.animator.SetBool("isMoving", false);
	}

	IEnumerator MoveSmoothly(Node step)
	{
		Coordinate direction = step.Coordinate - this.node.Coordinate;

		this.isMoving = true;
		this.animator.SetBool("isMoving", true);
		this.animator.SetFloat("xVelocity", direction.X);
		this.animator.SetFloat("yVelocity", direction.Y);

		float sqrRemainingDistance = (this.transform.position - step.Coordinate).sqrMagnitude;

		while (sqrRemainingDistance > float.Epsilon)
		{
			Vector3 newPosition = Vector3.MoveTowards(this.transform.position, step.Coordinate, this.moveTime * Time.deltaTime);
			rigidBody.MovePosition(newPosition);
			sqrRemainingDistance = (this.transform.position - step.Coordinate).sqrMagnitude;

			yield return null;
		}

		this.isMoving = false;
		this.animator.SetBool("isMoving", false);
	}
}