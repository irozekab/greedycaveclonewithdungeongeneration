﻿using UnityEngine;
using System.Collections.Generic;

namespace Kameosa
{
    public struct Coordinate
    {
        public static Coordinate Up
        {
            get { return new Coordinate(0, 1); }
        }

        public static Coordinate Right
        {
            get { return new Coordinate(1, 0); }
        }

        public static Coordinate Down
        {
            get { return new Coordinate(0, -1); }
        }

        public static Coordinate Left
        {
            get { return new Coordinate(-1, 0); }
        }

        public static List<Coordinate> Directions
        {
            get
            {
                List<Coordinate> directions = new List<Coordinate>();

                directions.Add(Coordinate.Up);
                directions.Add(Coordinate.Down);
                directions.Add(Coordinate.Left);
                directions.Add(Coordinate.Right);

                return directions;
            }
        }

        public int X { get; set; }
        public int Y { get; set; }

        public int sqrMagnitude
        {
            get
            {
                return (this.X * this.X) + (this.Y * this.Y);
            }
        }

        public List<Coordinate> Neighbours
        {
            get
            {
                List<Coordinate> neighbours = new List<Coordinate>();

                foreach (Coordinate direction in Coordinate.Directions)
                {
                    neighbours.Add(this + direction);
                }

                return neighbours;
            }
        }

        public Coordinate(int x, int y)
        {
            this.X = x;
            this.Y = y;
        }

        public static implicit operator Vector2(Coordinate coordinate)
        {
            return new Vector2(coordinate.X, coordinate.Y);
        }

        public static implicit operator Vector3(Coordinate coordinate)
        {
            return new Vector3(coordinate.X, coordinate.Y, 0);
        }

        public static implicit operator Coordinate(Vector2 coordinate)
        {
            return new Coordinate((int)coordinate.x, (int)coordinate.y);
        }

        public static implicit operator string(Coordinate coordinate)
        {
            return "(" + coordinate.X + ", " + coordinate.Y + ")";
        }

        public static Coordinate operator +(Coordinate a, Coordinate b)
        {
            return new Coordinate(a.X + b.X, a.Y + b.Y);
        }

        public static Coordinate operator +(Coordinate a, Vector2 b)
        {
            return new Coordinate(a.X + (int)b.x, a.Y + (int)b.y);
        }

        public static Coordinate operator -(Coordinate a, Coordinate b)
        {
            return new Coordinate(a.X - b.X, a.Y - b.Y);
        }

        public static Coordinate operator -(Coordinate a, Vector2 b)
        {
            return new Coordinate(a.X - (int)b.x, a.Y - (int)b.y);
        }

        public static Coordinate operator *(Coordinate a, int b)
        {
            return new Coordinate(a.X * b, a.Y * b);
        }

        /*
        public static bool operator ==(Coordinate a, Coordinate b)
        {
            return (a.X == b.X && a.Y == b.Y);
        }

        public static bool operator !=(Coordinate a, Coordinate b)
        {
            return (a.X != b.X || a.Y != b.Y);
        }
        */

        public bool IsAbove(Coordinate coordinate)
        {
            return this.X > coordinate.X;
        }

        public bool IsBelow(Coordinate coordinate)
        {
            return this.X < coordinate.X;
        }

        public bool IsRightOf(Coordinate coordinate)
        {
            return this.Y > coordinate.Y;
        }

        public bool IsLeftOf(Coordinate coordinate)
        {
            return this.Y < coordinate.Y;
        }

        public bool Equals(Coordinate coordinate)
        {
            return (this.X == coordinate.X) && (this.Y == coordinate.Y);
        }

        public bool NotEquals(Coordinate coordinate)
        {
            return !this.Equals(coordinate);
        }

        public override int GetHashCode()
        {
            return this.X ^ this.Y;
        }

        public override string ToString()
        {
            return string.Format("({1}, {2})", this.X, this.Y);
        }
    }
}