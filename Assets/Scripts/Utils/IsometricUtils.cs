﻿using UnityEngine;

/**
 * INFO
 * 
 * The standard cartesian to isometric formula seems to be (x - y, (x + y) / 2).
 * The alternative formula will be (x + y, (y - x) / 2).
 */
namespace Kameosa
{
	public class IsometricUtils
	{
		/**
		 * Cartesian to Isometric projection
		 */
		public static Vector2 CartesianToIsometricVector2(float x, float y)
		{
			// return new Vector2(x - y, (x + y) / 2);
			return new Vector2(x + y, (y - x) / 2);
		}

		public static Vector2 CartesianToIsometricVector2(Vector2 vector2)
		{
			return IsometricUtils.CartesianToIsometricVector2(vector2.x, vector2.y);
		}

		public static Vector2 CartesianToIsometricVector2(Vector3 vector3)
		{
			return IsometricUtils.CartesianToIsometricVector2(vector3.x, vector3.y);
		}

		public static Vector2 CartesianToIsometricVector2(Coordinate coordinate)
		{
			return IsometricUtils.CartesianToIsometricVector2(coordinate.X, coordinate.Y);
		}

		public static Vector3 CartesianToIsometricVector3(float x, float y)
		{
			Vector2 vector2 = IsometricUtils.CartesianToIsometricVector2(x, y);

			return new Vector3(vector2.x, vector2.y);
		}

		public static Vector3 CartesianToIsometricVector3(Vector2 vector2)
		{
			return IsometricUtils.CartesianToIsometricVector3(vector2.x, vector2.y);
		}

		public static Vector3 CartesianToIsometricVector3(Vector3 vector3)
		{
			return IsometricUtils.CartesianToIsometricVector3(vector3.x, vector3.y);
		}

		public static Vector3 CartesianToIsometricVector3(Coordinate coordinate)
		{
			return IsometricUtils.CartesianToIsometricVector3(coordinate.X, coordinate.Y);
		}

		/**
		 * Isometric to Cartesian projection
		 */
		public static Vector2 IsometricToCartesianVector2(float x, float y)
		{
			// return new Vector2(((2 * y) + x) / 2, ((2 * y) - x) / 2);
			return new Vector2((x / 2) - y, y + (x / 2));
		}

		public static Vector2 IsometricToCartesianVector2(Vector2 vector2)
		{
			return IsometricUtils.IsometricToCartesianVector2(vector2.x, vector2.y);
		}

		public static Vector2 IsometricToCartesianVector2(Vector3 vector3)
		{
			return IsometricUtils.IsometricToCartesianVector2(vector3.x, vector3.y);
		}
	}
}