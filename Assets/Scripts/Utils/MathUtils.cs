﻿using UnityEngine;

namespace Kameosa
{
	public class MathUtils
	{
		public static int GetRandomOddIntegerWithinRange(int min, int max)
		{
			return (Mathf.FloorToInt(Random.Range(min / 2, max / 2)) * 2) + 1;
		}

		public static int CeilingToOdd(int number)
		{
			if (number % 2 == 1) {
				return number;
			}

			return number + 1;
		}
	}
}