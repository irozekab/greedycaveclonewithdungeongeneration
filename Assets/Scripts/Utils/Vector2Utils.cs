﻿using UnityEngine;
using System.Collections.Generic;

namespace Kameosa
{
    public static class Vector2Utils
    {
        public static Vector2 Vector2FromAngle(float angle)
        {
            angle *= Mathf.Deg2Rad;

            return new Vector2(Mathf.Cos(angle), Mathf.Sin(angle));
        }
    }
}