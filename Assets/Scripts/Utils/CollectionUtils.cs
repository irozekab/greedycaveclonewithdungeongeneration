﻿using System.Collections.Generic;
using UnityEngine;

namespace Kameosa
{
    public class CollectionUtils
    {
        public static void Randomize<T>(List<T> list)
        {
            for (int i = 0; i < list.Count; i++)
            {
                T temp = list[i];
                int randomIndex = Random.Range(i, list.Count);
                list[i] = list[randomIndex];
                list[randomIndex] = temp;
            }
        }

        public static T GetRandomFromList<T>(List<T> list)
        {
            return list[Random.Range(0, list.Count)];
        }

        public static T GetMiddleFromList<T>(List<T> list)
        {
		return list[Mathf.FloorToInt(list.Count / 2)];
        }
    }
}