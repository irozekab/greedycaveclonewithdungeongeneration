﻿using UnityEngine;
using System.Collections;
using System;

namespace Kameosa
{
    public class Heap<T> where T : IHeapNode<T>
    {
        T[] nodes;
        int currentNodeCount;

        public int Count
        {
            get { return currentNodeCount; }
        }

        public Heap(int maxHeapSize)
        {
            nodes = new T[maxHeapSize];
        }

        public void Add(T node)
        {
            node.HeapIndex = currentNodeCount;
            nodes[currentNodeCount] = node;

            SortUp(node);

            currentNodeCount++;
        }

        public T RemoveFirst()
        {
            T firstNode = nodes[0];
            currentNodeCount--;
            nodes[0] = nodes[currentNodeCount];
            nodes[0].HeapIndex = 0;

            SortDown(nodes[0]);

            return firstNode;
        }

        public void UpdateNode(T node)
        {
            SortUp(node);
        }

        public bool Contains(T node)
        {
            return Equals(nodes[node.HeapIndex], node);
        }

        void SortDown(T node)
        {
            while (true)
            {
                int childIndexLeft = (node.HeapIndex * 2) + 1;
                int childIndexRight = (node.HeapIndex * 2) + 2;
                int swapIndex = 0;

                if (childIndexLeft < currentNodeCount)
                {
                    swapIndex = childIndexLeft;

                    if (childIndexRight < currentNodeCount)
                    {
                        if (nodes[childIndexLeft].CompareTo(nodes[childIndexRight]) < 0)
                        {
                            swapIndex = childIndexRight;
                        }
                    }

                    if (node.CompareTo(nodes[swapIndex]) < 0)
                    {
                        Swap(node, nodes[swapIndex]);
                    }
                    else
                    {
                        return;
                    }

                }
                else
                {
                    return;
                }
            }
        }

        void SortUp(T node)
        {
            int parentIndex = (node.HeapIndex - 1) / 2;

            while (true)
            {
                T parentNode = nodes[parentIndex];

                if (node.CompareTo(parentNode) > 0)
                {
                    Swap(node, parentNode);
                }
                else
                {
                    break;
                }

                parentIndex = (node.HeapIndex - 1) / 2;
            }
        }

        void Swap(T nodeA, T nodeB)
        {
            nodes[nodeA.HeapIndex] = nodeB;
            nodes[nodeB.HeapIndex] = nodeA;

            int nodeAIndex = nodeA.HeapIndex;

            nodeA.HeapIndex = nodeB.HeapIndex;
            nodeB.HeapIndex = nodeAIndex;
        }
    }

    public interface IHeapNode<T> : IComparable<T>
    {
        int HeapIndex { get; set; }
    }
}