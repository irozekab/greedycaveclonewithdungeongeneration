﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Kameosa;
using DungeonGeneration;

public class DungeonManager : MonoBehaviour
{
    private const float NODE_GIZMO_SIZE = 0.4f;

    public bool isShowGizmo = true;
    public int width = 81;
    public int height = 61;
    [Range(0, 100)] public int roomCount = 20;
    [Range(3, 10)] public int minRoomSize = 3;
    [Range(10, 30)] public int maxRoomSize = 10;
    [Range(0, 0.2f)] public float additionalLinkProbability = 0.025f;
    [Range(0, 1)] public float deadEndPercentage = 0.5f;

    public NodeInstantiator nodeInstantiator;
    public MobileInstantiator mobileInstantiator;
    public FixtureInstantiator fixtureInstantiator;

    private Dungeon dungeon;

    void Start()
    {
        Generate();
    }

    void Update()
    {
    }

    void Generate()
    {
        this.dungeon = DungeonGenerator.Generate(this.width, this.height, this.roomCount, this.minRoomSize, this.maxRoomSize, this.additionalLinkProbability, this.deadEndPercentage);
        DungeonPopulator.Populate(this.dungeon);

        this.nodeInstantiator.Instantiate(this.dungeon);
        this.mobileInstantiator.Instantiate(this.dungeon);
        this.fixtureInstantiator.Instantiate(this.dungeon);
    }

    void OnDrawGizmos()
    {
        if ((this.dungeon != null) && this.isShowGizmo)
        {
            float nodeSize = 1f;
            foreach (Node node in this.dungeon)
            {
                Vector3 position = new Vector3(node.X * nodeSize, node.Y * nodeSize, 0);

                switch (node.Type)
                {
                    case NodeType.SOLID:
                        Gizmos.color = Color.black;
                        break;
                    case NodeType.CARVED:
                        Gizmos.color = Color.white;
                        break;
                }

                Gizmos.DrawCube(position, Vector3.one * NODE_GIZMO_SIZE);
            }
        }
    }
}