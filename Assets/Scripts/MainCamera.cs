﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainCamera : MonoBehaviour
{
    public Transform followTarget;
    void Start()
    {
    }

    void Update()
    {
        Camera.main.transform.position = new Vector3(followTarget.position.x, followTarget.position.y, Camera.main.transform.position.z);
    }
}
