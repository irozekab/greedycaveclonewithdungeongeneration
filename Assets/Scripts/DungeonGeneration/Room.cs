﻿using System.Collections;
using System.Collections.Generic;
using Kameosa;

/**
 * INFO:
 * 
 * X and Y will represent the bottom left node of the room. Works better with Unity.
 */
namespace DungeonGeneration
{
    public struct Room : IEnumerable
    {
        public Coordinate BottomLeft { get; }
        public int Width { get; }
        public int Height { get; }

        public int X 
        {
            get { return this.BottomLeft.X; }
        }
        public int Y 
        {
            get { return this.BottomLeft.Y; }
        }

        public Coordinate TopRight
        {
            get { return new Coordinate(this.X + this.Width - 1, this.Y + this.Height - 1); }
        }

        public List<Node> Nodes { get; }

        public Room(Coordinate bottomLeft, int width, int height)
        {
            this.BottomLeft = bottomLeft;
            this.Width = width;
            this.Height = height;
            this.Nodes = new List<Node>();
        }

        public Room(int x, int y, int width, int height)
            : this(new Coordinate(x, y), width, height)
        { }

        public IEnumerator GetEnumerator()
        {
            for (int x = this.X; x < this.X + this.Width; x++)
            {
                for (int y = this.Y; y < this.Y + this.Height; y++)
                {
                    yield return new Coordinate(x, y);
                }
            }
        }

        public IEnumerable GetCoordinates()
        {
            for (int x = this.X; x < this.X + this.Width; x++)
            {
                for (int y = this.Y; y < this.Y + this.Height; y++)
                {
                    yield return new Coordinate(x, y);
                }
            }
        }

        public bool IsAbove(Room room)
        {
            return this.BottomLeft.Y > room.TopRight.Y;
        }

        public bool IsBelow(Room room)
        {
            return this.TopRight.Y < room.BottomLeft.Y;
        }

        public bool IsRightOf(Room room)
        {
            return this.BottomLeft.X > room.TopRight.X;
        }

        public bool IsLeftOf(Room room)
        {
            return this.TopRight.X < room.BottomLeft.X;
        }

        public bool IsOverlappedWithRoom(Room room)
        {
            return !(this.IsAbove(room) || this.IsBelow(room) || this.IsRightOf(room) || this.IsLeftOf(room));
        }

        public bool IsOverlappedWithRooms(List<Room> rooms)
        {
            foreach (Room otherRoom in rooms)
            {
                if (this.IsOverlappedWithRoom(otherRoom))
                {
                    return true;
                }
            }

            return false;
        }
    }
}