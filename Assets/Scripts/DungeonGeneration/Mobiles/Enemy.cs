﻿using Kameosa;
using System.Collections.Generic;
using UnityEngine;

/**
 * INFO:
 * 
 */
namespace DungeonGeneration
{
    public class Enemy : MobileBase
    {
        public EnemyType Type { get; set; }

        public Enemy(Node node, EnemyType type) : base(node)
        {
            this.Type = type;
        }
    }
}