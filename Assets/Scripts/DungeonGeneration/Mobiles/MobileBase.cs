﻿using Kameosa;
using System.Collections.Generic;
using UnityEngine;

/**
 * INFO:
 * 
 */
namespace DungeonGeneration
{
    public abstract class MobileBase 
    {
        private static int count = 0;

        public Node Node { get; }
        public string Id { get; }

        public MobileBase(Node node)
        {
            this.Node = node;
            this.Id = string.Format("Mobile#{0}", MobileBase.count.ToString("D10")); 

            MobileBase.count++;
        }
    }
}