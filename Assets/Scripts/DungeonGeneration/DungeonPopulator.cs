﻿using System.Linq;
using System.Diagnostics;
using System.Collections.Generic;
using UnityEngine;
using Kameosa;

/**
 * INFO:
 * 
 */

namespace DungeonGeneration
{
    public static class DungeonPopulator
    {
        private static Dungeon dungeon;

        public static void Populate(Dungeon dungeon)
        {
#if UNITY_EDITOR
            List<long> time = new List<long>();
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
#endif
            DungeonPopulator.dungeon = dungeon;

#if UNITY_EDITOR
            stopwatch.Stop();
            time.Add(stopwatch.ElapsedMilliseconds - time.Sum());
            stopwatch.Start();
#endif

            SetStartAndEnd();

#if UNITY_EDITOR
            stopwatch.Stop();
            time.Add(stopwatch.ElapsedMilliseconds - time.Sum());
            stopwatch.Start();
#endif

            PopulateWithEnemies();

#if UNITY_EDITOR
            stopwatch.Stop();
            time.Add(stopwatch.ElapsedMilliseconds - time.Sum());
            stopwatch.Start();
#endif

            PopulateWithFixtures();

#if UNITY_EDITOR
            stopwatch.Stop();
            time.Add(stopwatch.ElapsedMilliseconds - time.Sum());

            UnityEngine.Debug.Log("<color=yellow>DungeonGeneration::DungeonPopulator</color>");
            UnityEngine.Debug.Log(string.Format("Generate() Initializing data structures: {0} ms.", time[0]));
            UnityEngine.Debug.Log(string.Format("Populate() SetStartAndEnd: {0} ms.", time[1]));
            UnityEngine.Debug.Log(string.Format("Populate() PopulateWithEnemies: {0} ms.", time[2]));
            UnityEngine.Debug.Log(string.Format("Populate() PopulateWithFixtures: {0} ms.", time[3]));
            UnityEngine.Debug.Log(string.Format("Populate() Total: {0} ms.", stopwatch.ElapsedMilliseconds));
#endif
        }

        static void SetStartAndEnd()
        {
            List<Room> rooms = new List<Room>();

            DungeonPopulator.dungeon.Rooms.Sort(delegate (Room a, Room b) { return (a.X + a.Y) - (b.X + b.Y); });

            rooms.Add(DungeonPopulator.dungeon.Rooms.First());
            rooms.Add(DungeonPopulator.dungeon.Rooms.Last());

            DungeonPopulator.dungeon.Rooms.Sort(delegate (Room a, Room b) { return (a.X - a.Y) - (b.X - b.Y); });

            rooms.Add(DungeonPopulator.dungeon.Rooms.First());
            rooms.Add(DungeonPopulator.dungeon.Rooms.Last());

            CollectionUtils.Randomize<Room>(rooms);

            Node entranceNode = CollectionUtils.GetMiddleFromList(rooms.First().Nodes);
            Node exitNode = CollectionUtils.GetMiddleFromList(rooms.Last().Nodes);

            DungeonPopulator.dungeon.Player = new Player(entranceNode);
            DungeonPopulator.dungeon.Exit = new Exit(exitNode);
        }

        static void PopulateWithEnemies()
        {
            for (int i = 0; i < 10; i++)
            {
                Node node = CollectionUtils.GetRandomFromList(DungeonPopulator.dungeon.CorridorNodes);
                DungeonPopulator.dungeon.Enemies.Add(new Enemy(node, EnemyType.GOBLIN));
            }
        }

        static void PopulateWithFixtures()
        {
        }

        #region Helpers

        #endregion
    }
}