﻿using System.Collections;
using System.Diagnostics;
using System.Collections.Generic;
using Kameosa;

namespace DungeonGeneration
{
    public class Dungeon : IEnumerable
    {
        public List<Node> CorridorNodes { get; }
        public List<Room> Rooms { get; }

        public Player Player { get; set; }
        public Exit Exit { get; set; }
        public List<Enemy> Enemies { get; }

        public int Width
        {
            get { return this.nodes.GetLength(0); }
        }

        public int Height
        {
            get { return this.nodes.GetLength(1); }
        }

        private Node[,] nodes;

        public Dungeon(int width, int height)
        {
            width = MathUtils.CeilingToOdd(width);
            height = MathUtils.CeilingToOdd(height);

            this.nodes = new Node[width, height];

            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    this.nodes[x, y] = new Node(this, x, y);
                }
            }

            this.Rooms = new List<Room>();
            this.CorridorNodes = new List<Node>();
            this.Enemies = new List<Enemy>();
        }

        public Node GetNode(Coordinate coordinate)
        {
            return GetNode(coordinate.X, coordinate.Y);
        }

        public Node GetNode(int x, int y)
        {
            if (IsCoordinateWithinDungeon(x, y)) {
                return this.nodes[x, y];
            }

            return null;
        }

        public bool IsCoordinateWithinDungeon(Coordinate coordinate)
        {
            return IsCoordinateWithinDungeon(coordinate.X, coordinate.Y);
        }

        public bool IsCoordinateWithinDungeon(int x, int y)
        {
            return x >= 0
                && x < this.Width
                && y >= 0
                && y < this.Height;
        }

        public IEnumerator GetEnumerator()
        {
            for (int x = 0; x < this.nodes.GetLength(0); x++)
            {
                for (int y = 0; y < this.nodes.GetLength(1); y++)
                {
                    yield return this.nodes[x, y];
                }
            }
        }

        public static Stack<Node> GetPath(Node startNode, Node endNode)
        {
            #if UNITY_EDITOR
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            #endif

            Heap<Node> openSet = new Heap<Node>(100);
            HashSet<Node> closedSet = new HashSet<Node>();

            openSet.Add(startNode);

            while (openSet.Count > 0)
            {
                Node currentNode = openSet.RemoveFirst();
                closedSet.Add(currentNode);

                if (currentNode == endNode)
                {
                    break;
                }

                foreach (Node neighbourNode in currentNode.GetNeighbourNodes())
                {
                    if (!neighbourNode.IsWalkable || closedSet.Contains(neighbourNode))
                    {
                        continue;
                    }

                    int newMovementCostToNeightbour = currentNode.GCost + currentNode.GetDistanceToNode(neighbourNode);

                    if (newMovementCostToNeightbour < neighbourNode.GCost || !openSet.Contains(neighbourNode))
                    {
                        neighbourNode.GCost = newMovementCostToNeightbour;
                        neighbourNode.HCost = neighbourNode.GetDistanceToNode(endNode);
                        neighbourNode.previousStep = currentNode;

                        if (!openSet.Contains(neighbourNode))
                        {
                            openSet.Add(neighbourNode);
                        }
                        else
                        {
                            openSet.UpdateNode(neighbourNode);
                        }
                    }
                }
            }

            Stack<Node> path = new Stack<Node>();
            Node currentPathNode = endNode;

            while (currentPathNode != startNode)
            {
                path.Push(currentPathNode);
                currentPathNode = currentPathNode.previousStep;
            }

            #if UNITY_EDITOR
            stopwatch.Stop();

            UnityEngine.Debug.Log("<color=red>DungeonGeneration::Dungeon</color>");
            UnityEngine.Debug.Log(string.Format("GetPath() Total: {0} ms.", stopwatch.ElapsedMilliseconds));
            #endif

            return path;
        }
    }
}