﻿using Kameosa;
using System.Collections.Generic;
using UnityEngine;

/**
 * INFO:
 * 
 */
namespace DungeonGeneration
{
    public abstract class FixtureBase 
    {
        private static int count = 0;

        public Node Node { get; }
        public string Id { get; }

        public FixtureBase(Node node)
        {
            this.Node = node;
            this.Id = string.Format("Fixture#{0}", FixtureBase.count.ToString("D10")); 

            FixtureBase.count++;
        }
    }
}