﻿using Kameosa;
using System.Collections.Generic;
using UnityEngine;

/**
 * INFO:
 * 
 * Struct because we do not need it as a reference. This allows faster accessing. 
 */
namespace DungeonGeneration
{
    public class Node : IHeapNode<Node>
    {
        public Node previousStep;

        public Dungeon Dungeon { get; }
        public Coordinate Coordinate { get; }
        public NodeType Type { get; set; }

        public int HeapIndex { get; set; }
        public int GCost { get; set; }
        public int HCost { get; set; }

        public int FCost
        {
            get { return this.GCost + this.HCost; }
        }

        public bool IsWalkable
        {
            get
            {
                return this.Type == NodeType.CARVED;
            }
        }

        public int X
        {
            get { return this.Coordinate.X; }
        }

        public int Y
        {
            get { return this.Coordinate.Y; }
        }

        public Node(Dungeon dungeon, Coordinate coordinate, NodeType type = NodeType.SOLID)
        {
            this.Dungeon = dungeon;
            this.Coordinate = coordinate;
            this.Type = type;

            this.GCost = 0;
            this.HCost = 0;
        }

        public Node(Dungeon dungeon, int x, int y, NodeType type = NodeType.SOLID)
            : this(dungeon, new Coordinate(x, y), type)
        { }

        public int GetDistanceToNode(Node node)
        {
            int xDistance = Mathf.Abs(this.X - node.X);
            int yDistance = Mathf.Abs(this.Y - node.Y);

            return yDistance + xDistance;
        }

        public int CompareTo(Node node)
        {
            int compare = this.FCost.CompareTo(node.FCost);

            if (compare == 0)
            {
                compare = this.HCost.CompareTo(node.HCost);
            }

            return -compare;
        }

        public override string ToString()
        {
            return string.Format("{0} at ({1}, {2})", this.Type.ToString(), this.X, this.Y);
        }

        public List<Node> GetNeighbourNodes()
        {
            List<Node> neighbourNodes = new List<Node>();

            foreach (Coordinate direction in Coordinate.Directions)
            {
                Node neighbourNode = this.Dungeon.GetNode(this.Coordinate + direction);

                if (neighbourNode != null)
                {
                    neighbourNodes.Add(neighbourNode);
                }
            }

            return neighbourNodes;
        }
    }
}