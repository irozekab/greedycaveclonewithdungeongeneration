﻿using System.Linq;
using System.Diagnostics;
using System.Collections.Generic;
using UnityEngine;
using Kameosa;

/**
 * INFO:
 * 
 * The random dungeon generator.
 * https://github.com/Itsykumo/hauberk
 *
 * 1. Initialize a dungeon with solid walls.
 *
 * 2. Place a number of randomly sized and positioned rooms. If a room
 *    overlaps an existing room, it is discarded. Any remaining rooms are
 *    carved out.
 *
 * 3. Any remaining solid areas are filled in with mazes. The maze generator
 *    will grow and fill in even odd-shaped areas, but will not touch any rooms.
 *
 * 4. The result of the previous two steps is a series of unconnected rooms
 *    and mazes. We walk the stage and find every node that can be a
 *    "connector". This is a solid node that is adjacent to two unconnected
 *    regions.
 *
 * 5. We randomly choose connectors and open them or place a door there until
 *    all of the unconnected regions have been joined. There is also a slight
 *    chance to carve a connector between two already-joined regions, so that
 *    the dungeon isn't single connected.
 *
 * 6. The mazes will have a lot of dead ends. Finally, we remove those by
 *    repeatedly filling in any open node that's closed on three sides. When
 *    this is done, every corridor in a maze actually leads somewhere.
 *
 * The end result of this is a multiply-connected dungeon with rooms and lots
 * of winding corridors.
 */

namespace DungeonGeneration
{
    public static class DungeonGenerator
    {
        private const int NO_REGION = 0;
        private const int TRY_CREATE_ROOM_MAX_ATTEMPT_COUNT = 20;
        private const int BOUNDARY = 1;

        private static Dungeon dungeon;

        private static int[,] regions;
        private static int currentRegion;
        private static Dictionary<int, int> regionMappings;

        private static int maxRoomCount;
        private static int minRoomSize;
        private static int maxRoomSize;

        private static float additionalLinkProbability;

        private static float deadEndPercentage;

        public static Dungeon Generate(int width, int height, int maxRoomCount, int minRoomSize, int maxRoomSize, float additionalLinkProbability, float deadEndPercentage)
        {
#if UNITY_EDITOR
            List<long> time = new List<long>();
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
#endif

            DungeonGenerator.dungeon = new Dungeon(width, height);

            DungeonGenerator.currentRegion = NO_REGION;
            DungeonGenerator.regions = new int[DungeonGenerator.dungeon.Width, DungeonGenerator.dungeon.Height];
            DungeonGenerator.regionMappings = new Dictionary<int, int>();

            DungeonGenerator.maxRoomCount = maxRoomCount;
            DungeonGenerator.minRoomSize = minRoomSize;
            DungeonGenerator.maxRoomSize = maxRoomSize;

            DungeonGenerator.additionalLinkProbability = additionalLinkProbability;
            DungeonGenerator.deadEndPercentage = deadEndPercentage;

#if UNITY_EDITOR
            stopwatch.Stop();
            time.Add(stopwatch.ElapsedMilliseconds);
            stopwatch.Start();
#endif

            GenerateRoomsRandomly();

#if UNITY_EDITOR
            stopwatch.Stop();
            time.Add(stopwatch.ElapsedMilliseconds - time.Sum());
            stopwatch.Start();
#endif

            GenerateCorridors();

#if UNITY_EDITOR
            stopwatch.Stop();
            time.Add(stopwatch.ElapsedMilliseconds - time.Sum());
            stopwatch.Start();
#endif

            LinkRegions();

#if UNITY_EDITOR
            stopwatch.Stop();
            time.Add(stopwatch.ElapsedMilliseconds - time.Sum());
            stopwatch.Start();
#endif

            RemoveDeadEnds();

#if UNITY_EDITOR
            stopwatch.Stop();
            time.Add(stopwatch.ElapsedMilliseconds - time.Sum());

            UnityEngine.Debug.Log("<color=yellow>DungeonGeneration::DungeonGenerator</color>");
            UnityEngine.Debug.Log(string.Format("Generate() Initializing data structures: {0} ms.", time[0]));
            UnityEngine.Debug.Log(string.Format("Generate() GenerateRoomsRandomly: {0} ms.", time[1]));
            UnityEngine.Debug.Log(string.Format("Generate() GenerateCorridors: {0} ms.", time[2]));
            UnityEngine.Debug.Log(string.Format("Generate() LinkRegions: {0} ms.", time[3]));
            UnityEngine.Debug.Log(string.Format("Generate() RemoveDeadEnds: {0} ms.", time[4]));
            UnityEngine.Debug.Log(string.Format("Generate() Total: {0} ms.", stopwatch.ElapsedMilliseconds));
#endif

            return DungeonGenerator.dungeon;
        }

        static void GenerateRoomsRandomly()
        {
            for (int i = 0; i < DungeonGenerator.maxRoomCount; i++)
            {
                int roomWidth = MathUtils.GetRandomOddIntegerWithinRange(DungeonGenerator.minRoomSize, DungeonGenerator.maxRoomSize);
                int roomHeight = MathUtils.GetRandomOddIntegerWithinRange(DungeonGenerator.minRoomSize, DungeonGenerator.maxRoomSize);

                for (int j = 0; j < TRY_CREATE_ROOM_MAX_ATTEMPT_COUNT; j++)
                {
                    int roomX = MathUtils.GetRandomOddIntegerWithinRange(1, DungeonGenerator.dungeon.Width - roomWidth);
                    int roomY = MathUtils.GetRandomOddIntegerWithinRange(1, DungeonGenerator.dungeon.Height - roomHeight);

                    Room room = new Room(roomX, roomY, roomWidth, roomHeight);

                    if (!room.IsOverlappedWithRooms(DungeonGenerator.dungeon.Rooms))
                    {
                        StartNewRegion();
                        DungeonGenerator.dungeon.Rooms.Add(room);

                        foreach (Coordinate coordinate in room)
                        {
                            Node currentNode = DungeonGenerator.dungeon.GetNode(coordinate);
                            room.Nodes.Add(currentNode);
                            currentNode.Type = NodeType.CARVED;
                            SetNodeToCurrentRegion(currentNode);
                        }

                        break;
                    }
                }
            }
        }

        static void GenerateCorridors()
        {
            for (int x = DungeonGenerator.BOUNDARY; x < DungeonGenerator.dungeon.Width - DungeonGenerator.BOUNDARY; x += 2)
            {
                for (int y = DungeonGenerator.BOUNDARY; y < DungeonGenerator.dungeon.Height - DungeonGenerator.BOUNDARY; y += 2)
                {
                    if (DungeonGenerator.dungeon.GetNode(x, y).Type == NodeType.SOLID)
                    {
                        FloodMazeAtNode(DungeonGenerator.dungeon.GetNode(x, y));
                    }
                }
            }
        }

        static void LinkRegions()
        {
            List<Coordinate> linkableCoordinates = GetLinkableCoordinates();
            HashSet<Coordinate> linkedCoordinateNeighbours = new HashSet<Coordinate>();

            CollectionUtils.Randomize(linkableCoordinates);

            foreach (Coordinate linkableCoordinate in linkableCoordinates)
            {
                if (linkedCoordinateNeighbours.Contains(linkableCoordinate))
                {
                    continue;
                }

                HashSet<int> mappedRegionIndices = new HashSet<int>();

                foreach (Coordinate neighbourCoordinate in linkableCoordinate.Neighbours)
                {
                    int neighbourRegion = GetRegionAtCoordinate(neighbourCoordinate);

                    if (neighbourRegion != DungeonGenerator.NO_REGION)
                    {
                        mappedRegionIndices.Add(DungeonGenerator.regionMappings[neighbourRegion]);
                    }
                }

                bool hasMoreThanOneRegionLinkableWithThisCoordinate = mappedRegionIndices.Count >= 2;

                if (hasMoreThanOneRegionLinkableWithThisCoordinate)
                {
                    DungeonGenerator.dungeon.GetNode(linkableCoordinate).Type = NodeType.CARVED;

                    linkedCoordinateNeighbours.UnionWith(linkableCoordinate.Neighbours);

                    List<int> mappedRegionIndicesList = mappedRegionIndices.ToList();
                    int mergeToRegionIndex = mappedRegionIndicesList[0];
                    List<int> mergeFromRegionIndices = mappedRegionIndicesList.Skip(1).ToList();

                    foreach (int key in DungeonGenerator.regionMappings.Keys.ToList())
                    {
                        if (mergeFromRegionIndices.Contains(DungeonGenerator.regionMappings[key]))
                        {
                            DungeonGenerator.regionMappings[key] = mergeToRegionIndex;
                        }
                    }
                }
                else if (Random.value <= DungeonGenerator.additionalLinkProbability)
                {
                    DungeonGenerator.dungeon.GetNode(linkableCoordinate).Type = NodeType.CARVED;
                    linkedCoordinateNeighbours.UnionWith(linkableCoordinate.Neighbours);
                }
            }
        }

        static void RemoveDeadEnds()
        {
            int corridorsToLeave = Mathf.RoundToInt(DungeonGenerator.deadEndPercentage * DungeonGenerator.dungeon.CorridorNodes.Count);

            CollectionUtils.Randomize(DungeonGenerator.dungeon.CorridorNodes);

            while (DungeonGenerator.dungeon.CorridorNodes.Count > corridorsToLeave)
            {
                Node corridorNode = DungeonGenerator.dungeon.CorridorNodes[0];
                DungeonGenerator.dungeon.CorridorNodes.RemoveAt(0);

                if (corridorNode.Type != NodeType.SOLID)
                {
                    bool isDeadEnd = (corridorNode.GetNeighbourNodes().Count(neighbourNode => neighbourNode.Type != NodeType.SOLID) == 1);

                    if (isDeadEnd)
                    {
                        corridorNode.Type = NodeType.SOLID;

                        foreach (Node neighbourNode in corridorNode.GetNeighbourNodes().Where(neighbourNode => neighbourNode.Type == NodeType.CARVED))
                        {
                            DungeonGenerator.dungeon.CorridorNodes.Add(neighbourNode);
                        }
                    }
                }
            }
        }

        #region Helpers

        /**
         * INFO:
         * 
         * Growing tree algorithm.
         * http://www.astrolog.org/labyrnth/algrithm.htm.
         */
        static void FloodMazeAtNode(Node startingNode)
        {
            List<Node> holdingNodes = new List<Node>();

            StartNewRegion();
            startingNode.Type = NodeType.CARVED;
            SetNodeToCurrentRegion(startingNode);
            holdingNodes.Add(startingNode);

            while (holdingNodes.Count > 0)
            {
                Node currentNode = holdingNodes[holdingNodes.Count - 1];
                List<Coordinate> carvableDirections = GetCarvableDirections(currentNode);

                if (carvableDirections.Count > 0)
                {
                    Coordinate direction = carvableDirections[Random.Range(0, carvableDirections.Count)];

                    Node nodeToCarve = DungeonGenerator.dungeon.GetNode(currentNode.Coordinate + direction);

                    nodeToCarve.Type = NodeType.CARVED;
                    SetNodeToCurrentRegion(nodeToCarve);

                    nodeToCarve = DungeonGenerator.dungeon.GetNode(currentNode.Coordinate + (direction * 2));
                    nodeToCarve.Type = NodeType.CARVED;
                    SetNodeToCurrentRegion(nodeToCarve);

                    holdingNodes.Add(nodeToCarve);
                }
                else
                {
                    holdingNodes.Remove(currentNode);
                    DungeonGenerator.dungeon.CorridorNodes.Add(currentNode);
                }
            }
        }

        static void StartNewRegion()
        {
            DungeonGenerator.currentRegion++;
            DungeonGenerator.regionMappings.Add(DungeonGenerator.currentRegion, DungeonGenerator.currentRegion);
        }

        static void SetNodeToCurrentRegion(Node node)
        {
            DungeonGenerator.regions[node.X, node.Y] = DungeonGenerator.currentRegion;
        }

        static List<Coordinate> GetCarvableDirections(Node node)
        {
            List<Coordinate> carvableDirections = new List<Coordinate>();

            foreach (Coordinate direction in Coordinate.Directions)
            {
                Coordinate coordinate3StepsAhead = node.Coordinate + (direction * 3);
                Coordinate coordinate2StepsAhead = node.Coordinate + (direction * 2);

                if (DungeonGenerator.dungeon.IsCoordinateWithinDungeon(coordinate3StepsAhead)
                    && (DungeonGenerator.dungeon.GetNode(coordinate2StepsAhead).Type == NodeType.SOLID))
                {
                    carvableDirections.Add(direction);
                }
            }

            return carvableDirections;
        }

        static List<Coordinate> GetLinkableCoordinates()
        {
            List<Coordinate> linkableCoordinates = new List<Coordinate>();

            foreach (Node node in DungeonGenerator.dungeon)
            {
                if (node.Type == NodeType.SOLID)
                {
                    HashSet<int> regions = new HashSet<int>();

                    foreach (Node neighbourNode in node.GetNeighbourNodes())
                    {
                        regions.Add(GetRegionAtCoordinate(neighbourNode.Coordinate));
                    }

                    regions.Remove(GetRegionAtCoordinate(node.Coordinate));

                    if (regions.Count > 1)
                    {
                        linkableCoordinates.Add(node.Coordinate);
                    }
                }
            }

            return linkableCoordinates;
        }

        static int GetRegionAtCoordinate(Coordinate coordinate)
        {
            return DungeonGenerator.regions[coordinate.X, coordinate.Y];
        }

        static int GetRegionAtCoordinate(int x, int y)
        {
            return DungeonGenerator.regions[x, y];
        }

        #endregion
    }
}