﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Kameosa;
using DungeonGeneration;

public class MobileInstantiator : MonoBehaviour
{
    public GameObject playerPrefab;
    public GameObject enemyPrefab;

    private Dungeon dungeon;
    private Dictionary<EnemyType, GameObject> enemyDictionary;

    void Awake()
    {
        this.enemyDictionary = new Dictionary<EnemyType, GameObject>();

        this.enemyDictionary.Add(EnemyType.GOBLIN, this.enemyPrefab);
    }

    public void Instantiate(Dungeon dungeon)
    {
        foreach (Transform childTransform in this.transform)
        {
            GameObject.Destroy(childTransform.gameObject);
        }

        this.dungeon = dungeon;

        InstantiatePlayer();
        InstantiateEnemies();
    }

    private void InstantiatePlayer()
    {
        GameObject player = Instantiate(this.playerPrefab, dungeon.Player.Node.Coordinate, Quaternion.identity).gameObject;

        player.transform.SetParent(this.transform);
        player.GetComponent<MobileController>().node = dungeon.Player.Node;
    }

    private void InstantiateEnemies()
    {
        foreach (Enemy enemy in dungeon.Enemies)
        {
            GameObject enemyToInstantiate = this.enemyDictionary[enemy.Type];
            GameObject instantiatedEnemy = Instantiate(enemyToInstantiate, enemy.Node.Coordinate, Quaternion.identity).gameObject;

            instantiatedEnemy.transform.SetParent(this.transform);
            instantiatedEnemy.GetComponent<MobileController>().node = enemy.Node;
        }
    }
}