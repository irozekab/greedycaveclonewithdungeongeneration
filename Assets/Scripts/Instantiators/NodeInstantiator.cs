﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Kameosa;
using DungeonGeneration;

public class NodeInstantiator : MonoBehaviour
{
    enum PresentationTypeType
    {
        NULL,
        FLOOR,
        O000_WALL_NODE,
        O001_WALL_NODE,
        O004_WALL_NODE,
        O005_WALL_NODE,
        O007_WALL_NODE,
        O016_WALL_NODE,
        O019_WALL_NODE,
        O020_WALL_NODE,
        O021_WALL_NODE,
        O023_WALL_NODE,
        O028_WALL_NODE,
        O031_WALL_NODE,
        O064_WALL_NODE,
        O065_WALL_NODE,
        O068_WALL_NODE,
        O069_WALL_NODE,
        O071_WALL_NODE,
        O080_WALL_NODE,
        O081_WALL_NODE,
        O084_WALL_NODE,
        O085_WALL_NODE,
        O087_WALL_NODE,
        O092_WALL_NODE,
        O093_WALL_NODE,
        O095_WALL_NODE,
        O112_WALL_NODE,
        O113_WALL_NODE,
        O116_WALL_NODE,
        O117_WALL_NODE,
        O119_WALL_NODE,
        O124_WALL_NODE,
        O125_WALL_NODE,
        O127_WALL_NODE,
        O193_WALL_NODE,
        O197_WALL_NODE,
        O199_WALL_NODE,
        O209_WALL_NODE,
        O213_WALL_NODE,
        O215_WALL_NODE,
        O221_WALL_NODE,
        O223_WALL_NODE,
        O241_WALL_NODE,
        O245_WALL_NODE,
        O247_WALL_NODE,
        O253_WALL_NODE,
        O255_WALL_NODE
    }

    public GameObject floorNode;
    public GameObject O000WallNode;
    public GameObject O001WallNode;
    public GameObject O004WallNode;
    public GameObject O005WallNode;
    public GameObject O007WallNode;
    public GameObject O016WallNode;
    public GameObject O019WallNode;
    public GameObject O020WallNode;
    public GameObject O021WallNode;
    public GameObject O023WallNode;
    public GameObject O028WallNode;
    public GameObject O031WallNode;
    public GameObject O064WallNode;
    public GameObject O065WallNode;
    public GameObject O068WallNode;
    public GameObject O069WallNode;
    public GameObject O071WallNode;
    public GameObject O080WallNode;
    public GameObject O081WallNode;
    public GameObject O084WallNode;
    public GameObject O085WallNode;
    public GameObject O087WallNode;
    public GameObject O092WallNode;
    public GameObject O093WallNode;
    public GameObject O095WallNode;
    public GameObject O112WallNode;
    public GameObject O113WallNode;
    public GameObject O116WallNode;
    public GameObject O117WallNode;
    public GameObject O119WallNode;
    public GameObject O124WallNode;
    public GameObject O125WallNode;
    public GameObject O127WallNode;
    public GameObject O193WallNode;
    public GameObject O197WallNode;
    public GameObject O199WallNode;
    public GameObject O209WallNode;
    public GameObject O213WallNode;
    public GameObject O215WallNode;
    public GameObject O221WallNode;
    public GameObject O223WallNode;
    public GameObject O241WallNode;
    public GameObject O245WallNode;
    public GameObject O247WallNode;
    public GameObject O253WallNode;
    public GameObject O255WallNode;

    public Dungeon dungeon;

    private Dictionary<PresentationTypeType, GameObject> nodePresentationTypeDictionary;

    void Awake()
    {
        this.nodePresentationTypeDictionary = new Dictionary<PresentationTypeType, GameObject>();

        this.nodePresentationTypeDictionary.Add(PresentationTypeType.FLOOR, floorNode);
        this.nodePresentationTypeDictionary.Add(PresentationTypeType.O000_WALL_NODE, O000WallNode);
        this.nodePresentationTypeDictionary.Add(PresentationTypeType.O001_WALL_NODE, O001WallNode);
        this.nodePresentationTypeDictionary.Add(PresentationTypeType.O004_WALL_NODE, O004WallNode);
        this.nodePresentationTypeDictionary.Add(PresentationTypeType.O005_WALL_NODE, O005WallNode);
        this.nodePresentationTypeDictionary.Add(PresentationTypeType.O007_WALL_NODE, O007WallNode);
        this.nodePresentationTypeDictionary.Add(PresentationTypeType.O016_WALL_NODE, O016WallNode);
        this.nodePresentationTypeDictionary.Add(PresentationTypeType.O019_WALL_NODE, O019WallNode);
        this.nodePresentationTypeDictionary.Add(PresentationTypeType.O020_WALL_NODE, O020WallNode);
        this.nodePresentationTypeDictionary.Add(PresentationTypeType.O021_WALL_NODE, O021WallNode);
        this.nodePresentationTypeDictionary.Add(PresentationTypeType.O023_WALL_NODE, O023WallNode);
        this.nodePresentationTypeDictionary.Add(PresentationTypeType.O028_WALL_NODE, O028WallNode);
        this.nodePresentationTypeDictionary.Add(PresentationTypeType.O031_WALL_NODE, O031WallNode);
        this.nodePresentationTypeDictionary.Add(PresentationTypeType.O064_WALL_NODE, O064WallNode);
        this.nodePresentationTypeDictionary.Add(PresentationTypeType.O065_WALL_NODE, O065WallNode);
        this.nodePresentationTypeDictionary.Add(PresentationTypeType.O068_WALL_NODE, O068WallNode);
        this.nodePresentationTypeDictionary.Add(PresentationTypeType.O069_WALL_NODE, O069WallNode);
        this.nodePresentationTypeDictionary.Add(PresentationTypeType.O071_WALL_NODE, O071WallNode);
        this.nodePresentationTypeDictionary.Add(PresentationTypeType.O080_WALL_NODE, O080WallNode);
        this.nodePresentationTypeDictionary.Add(PresentationTypeType.O081_WALL_NODE, O081WallNode);
        this.nodePresentationTypeDictionary.Add(PresentationTypeType.O084_WALL_NODE, O084WallNode);
        this.nodePresentationTypeDictionary.Add(PresentationTypeType.O085_WALL_NODE, O085WallNode);
        this.nodePresentationTypeDictionary.Add(PresentationTypeType.O087_WALL_NODE, O087WallNode);
        this.nodePresentationTypeDictionary.Add(PresentationTypeType.O092_WALL_NODE, O092WallNode);
        this.nodePresentationTypeDictionary.Add(PresentationTypeType.O093_WALL_NODE, O093WallNode);
        this.nodePresentationTypeDictionary.Add(PresentationTypeType.O095_WALL_NODE, O095WallNode);
        this.nodePresentationTypeDictionary.Add(PresentationTypeType.O112_WALL_NODE, O112WallNode);
        this.nodePresentationTypeDictionary.Add(PresentationTypeType.O113_WALL_NODE, O113WallNode);
        this.nodePresentationTypeDictionary.Add(PresentationTypeType.O116_WALL_NODE, O116WallNode);
        this.nodePresentationTypeDictionary.Add(PresentationTypeType.O117_WALL_NODE, O117WallNode);
        this.nodePresentationTypeDictionary.Add(PresentationTypeType.O119_WALL_NODE, O119WallNode);
        this.nodePresentationTypeDictionary.Add(PresentationTypeType.O124_WALL_NODE, O124WallNode);
        this.nodePresentationTypeDictionary.Add(PresentationTypeType.O125_WALL_NODE, O125WallNode);
        this.nodePresentationTypeDictionary.Add(PresentationTypeType.O127_WALL_NODE, O127WallNode);
        this.nodePresentationTypeDictionary.Add(PresentationTypeType.O193_WALL_NODE, O193WallNode);
        this.nodePresentationTypeDictionary.Add(PresentationTypeType.O197_WALL_NODE, O197WallNode);
        this.nodePresentationTypeDictionary.Add(PresentationTypeType.O199_WALL_NODE, O199WallNode);
        this.nodePresentationTypeDictionary.Add(PresentationTypeType.O209_WALL_NODE, O209WallNode);
        this.nodePresentationTypeDictionary.Add(PresentationTypeType.O213_WALL_NODE, O213WallNode);
        this.nodePresentationTypeDictionary.Add(PresentationTypeType.O215_WALL_NODE, O215WallNode);
        this.nodePresentationTypeDictionary.Add(PresentationTypeType.O221_WALL_NODE, O221WallNode);
        this.nodePresentationTypeDictionary.Add(PresentationTypeType.O223_WALL_NODE, O223WallNode);
        this.nodePresentationTypeDictionary.Add(PresentationTypeType.O241_WALL_NODE, O241WallNode);
        this.nodePresentationTypeDictionary.Add(PresentationTypeType.O245_WALL_NODE, O245WallNode);
        this.nodePresentationTypeDictionary.Add(PresentationTypeType.O247_WALL_NODE, O247WallNode);
        this.nodePresentationTypeDictionary.Add(PresentationTypeType.O253_WALL_NODE, O253WallNode);
        this.nodePresentationTypeDictionary.Add(PresentationTypeType.O255_WALL_NODE, O255WallNode);
    }

    public void Instantiate(Dungeon dungeon)
    {
        this.dungeon = dungeon;

        foreach (Transform childTransform in this.transform)
        {
            GameObject.Destroy(childTransform.gameObject);
        }

        foreach (Node node in this.dungeon)
        {
            PresentationTypeType nodePresentationType = GetPresentationTypeType(node.Coordinate);

            if (this.nodePresentationTypeDictionary.ContainsKey(nodePresentationType))
            {
                GameObject nodePresentationTypeToInstantiate = this.nodePresentationTypeDictionary[nodePresentationType];
                GameObject instantiatedNodePresentationType = Instantiate(nodePresentationTypeToInstantiate, node.Coordinate, Quaternion.identity).gameObject;

                instantiatedNodePresentationType.GetComponent<NodeController>().node = node;
                instantiatedNodePresentationType.transform.SetParent(this.transform);
            }
        }
    }

	bool IsNodeAtCoordinateCarved(Coordinate coordinate)
	{
		return this.dungeon.GetNode(coordinate) != null && this.dungeon.GetNode(coordinate).Type == NodeType.CARVED;
	}

	string GetWallConfiguration(Coordinate coordinate)
	{
        int[] configurationArray = new int[] { 1, 1, 1, 1, 1, 1, 1, 1 };

        if (IsNodeAtCoordinateCarved(coordinate + Coordinate.Up + Coordinate.Left)) 
        {
            configurationArray[0] = 0;
        }

        if (IsNodeAtCoordinateCarved(coordinate + Coordinate.Up)) 
        {
            configurationArray[1] = 0;
        }

        if (IsNodeAtCoordinateCarved(coordinate + Coordinate.Up + Coordinate.Right)) 
        {
            configurationArray[2] = 0;
        }

        if (IsNodeAtCoordinateCarved(coordinate + Coordinate.Right)) 
        {
            configurationArray[3] = 0;
        }

        if (IsNodeAtCoordinateCarved(coordinate + Coordinate.Down + Coordinate.Right)) 
        {
            configurationArray[4] = 0;
        }

        if (IsNodeAtCoordinateCarved(coordinate + Coordinate.Down)) 
        {
            configurationArray[5] = 0;
        }

        if (IsNodeAtCoordinateCarved(coordinate + Coordinate.Down + Coordinate.Left)) 
        {
            configurationArray[6] = 0;
        }

        if (IsNodeAtCoordinateCarved(coordinate + Coordinate.Left)) 
        {
            configurationArray[7] = 0;
        }

        string configuration = "";

        foreach (int bit in configurationArray)
        {
            configuration += bit.ToString();
        }

        return configuration;
	}

    PresentationTypeType GetPresentationTypeType(Coordinate coordinate)
    {
        if (this.dungeon.GetNode(coordinate).Type == NodeType.CARVED)
        {
            return PresentationTypeType.FLOOR;
        }

        string wallConfiguration = GetWallConfiguration(coordinate);

        switch (wallConfiguration)
        {
            //   "01111111"
            case "11111111":
                return PresentationTypeType.O000_WALL_NODE;

            //   "01111111"
            case "01111111":
                return PresentationTypeType.O001_WALL_NODE;

            //   "11011111"
            case "11011111":
                return PresentationTypeType.O004_WALL_NODE;

            //   "01011111"
            case "01011111":
                return PresentationTypeType.O005_WALL_NODE;

            //   "x0x11111"
            case "00011111":
            case "00111111":
            case "10011111":
            case "10111111":
                return PresentationTypeType.O007_WALL_NODE;

            //   "11110111"
            case "11110111":
                return PresentationTypeType.O016_WALL_NODE;

            //   "01x0x111"
            case "01000111":
            case "01001111":
            case "01100111":
            case "01101111":
                return PresentationTypeType.O019_WALL_NODE;

            //   "11010111"
            case "11010111":
                return PresentationTypeType.O020_WALL_NODE;

            //   "01010111"
            case "01010111":
                return PresentationTypeType.O021_WALL_NODE;

            //   "x0x10111"
            case "00010111":
            case "00110111":
            case "10010111":
            case "10110111":
                return PresentationTypeType.O023_WALL_NODE;

            //   "11x0x111"
            case "11000111":
            case "11001111":
            case "11100111":
            case "11101111":
                return PresentationTypeType.O028_WALL_NODE;

            //   "x000x111"
            case "00000111":
            case "00001111":
            case "10000111":
            case "10001111":
                return PresentationTypeType.O031_WALL_NODE;

            //   "11111101"
            case "11111101":
                return PresentationTypeType.O064_WALL_NODE;

            //   "01111101"
            case "01111101":
                return PresentationTypeType.O065_WALL_NODE;

            //   "01011101"
            case "01011101":
                return PresentationTypeType.O069_WALL_NODE;

            //   "11011101"
            case "11011101":
                return PresentationTypeType.O068_WALL_NODE;

            //   "x0x11101"
            case "00011101":
            case "00111101":
            case "10011101":
            case "10111101":
                return PresentationTypeType.O071_WALL_NODE;

            //   "11110101"
            case "11110101":
                return PresentationTypeType.O080_WALL_NODE;

            //   "01110101"
            case "01110101":
                return PresentationTypeType.O081_WALL_NODE;

            //   "11010101"
            case "11010101":
                return PresentationTypeType.O084_WALL_NODE;

            //   "01010101"
            case "01010101":
                return PresentationTypeType.O085_WALL_NODE;

            //   "x0x10101"
            case "00010101":
            case "00110101":
            case "10010101":
            case "10110101":
                return PresentationTypeType.O087_WALL_NODE;

            //   "11x0x101"
            case "11000101":
            case "11001101":
            case "11100101":
            case "11101101":
                return PresentationTypeType.O092_WALL_NODE;
                
            //   "01x0x101"
            case "01000101":
            case "01001101":
            case "01100101":
            case "01101101":
                return PresentationTypeType.O093_WALL_NODE;

            //   "x0x0x101"
            case "00000101":
            case "00001101":
            case "00100101":
            case "00101101":
            case "10000101":
            case "10001101":
            case "10100101":
            case "10101101":
                return PresentationTypeType.O095_WALL_NODE;

            //   "1111x0x1"
            case "11110001":
            case "11110011":
            case "11111001":
            case "11111011":
                return PresentationTypeType.O112_WALL_NODE;

            //   "0111x0x1"
            case "01110001":
            case "01110011":
            case "01111001":
            case "01111011":
                return PresentationTypeType.O113_WALL_NODE;

            //   "1101x0x1"
            case "11010001":
            case "11010011":
            case "11011001":
            case "11011011":
                return PresentationTypeType.O116_WALL_NODE;

            //   "0101x0x1"
            case "01010001":
            case "01010011":
            case "01011001":
            case "01011011":
                return PresentationTypeType.O117_WALL_NODE;

            //   "x0x1x0x1"
            case "00010001":
            case "00010011":
            case "00011001":
            case "00011011":
            case "00110001":
            case "00110011":
            case "00111001":
            case "00111011":
            case "10010001":
            case "10010011":
            case "10011001":
            case "10011011":
            case "10110001":
            case "10110011":
            case "10111001":
            case "10111011":
                return PresentationTypeType.O119_WALL_NODE;

            //   "11x000x1"
            case "11000001":
            case "11000011":
            case "11100001":
            case "11100011":
                return PresentationTypeType.O124_WALL_NODE;

            //   "01x0x0x1"
            case "01000001":
            case "01000011":
            case "01001001":
            case "01001011":
            case "01100001":
            case "01100011":
            case "01101001":
            case "01101011":
                return PresentationTypeType.O125_WALL_NODE;

            //   "x0x0x0x1"
            case "00000001":
            case "00000011":
            case "00001001":
            case "00001011":
            case "00100001":
            case "00100011":
            case "00101001":
            case "00101011":
            case "10000001":
            case "10000011":
            case "10001001":
            case "10001011":
            case "10100001":
            case "10100011":
            case "10101001":
            case "10101011":
                return PresentationTypeType.O127_WALL_NODE;

            //   "x11111x0"
            case "01111100":
            case "01111110":
            case "11111100":
            case "11111110":
                return PresentationTypeType.O193_WALL_NODE;

            //   "x10111x0"
            case "01011100":
            case "01011110":
            case "11011100":
            case "11011110":
                return PresentationTypeType.O197_WALL_NODE;

            //   "00x111x0"
            case "00011100":
            case "00011110":
            case "00111100":
            case "00111110":
                return PresentationTypeType.O199_WALL_NODE;

            //   "x11101x0"
            case "01110100":
            case "01110110":
            case "11110100":
            case "11110110":
                return PresentationTypeType.O209_WALL_NODE;

            //   "x10101x0"
            case "01010100":
            case "01010110":
            case "11010100":
            case "11010110":
                return PresentationTypeType.O213_WALL_NODE;

            //   "x0x101x0"
            case "00010100":
            case "00010110":
            case "00110100":
            case "00110110":
            case "10010100":
            case "10010110":
            case "10110100":
            case "10110110":
                return PresentationTypeType.O215_WALL_NODE;

            //   "x1x0x1x0"
            case "01000100":
            case "01000110":
            case "01001100":
            case "01001110":
            case "01100100":
            case "01100110":
            case "01101100":
            case "01101110":
            case "11000100":
            case "11000110":
            case "11001100":
            case "11001110":
            case "11100100":
            case "11100110":
            case "11101100":
            case "11101110":
                return PresentationTypeType.O221_WALL_NODE;

            //   "x0x0x1x0"
            case "00000100":
            case "00000110":
            case "00001100":
            case "00001110":
            case "00100100":
            case "00100110":
            case "00101100":
            case "00101110":
            case "10000100":
            case "10000110":
            case "10001100":
            case "10001110":
            case "10100100":
            case "10100110":
            case "10101100":
            case "10101110":
                return PresentationTypeType.O223_WALL_NODE;

            //   "x111x0x0"
            case "01110000":
            case "01110010":
            case "01111000":
            case "01111010":
            case "11110000":
            case "11110010":
            case "11111000":
            case "11111010":
                return PresentationTypeType.O241_WALL_NODE;

            //   "x101x0x0"
            case "01010000":
            case "01010010":
            case "01011000":
            case "01011010":
            case "11010000":
            case "11010010":
            case "11011000":
            case "11011010":
                return PresentationTypeType.O245_WALL_NODE;

            //   "x0x1x0x0"
            case "00010000":
            case "00010010":
            case "00011000":
            case "00011010":
            case "00110000":
            case "00110010":
            case "00111000":
            case "00111010":
            case "10010000":
            case "10010010":
            case "10011000":
            case "10011010":
            case "10110000":
            case "10110010":
            case "10111000":
            case "10111010":
                return PresentationTypeType.O247_WALL_NODE;

            //   "x1x0x0x0"
            case "01000000":
            case "01000010":
            case "01001000":
            case "01001010":
            case "01100000":
            case "01100010":
            case "01101000":
            case "01101010":
            case "11000000":
            case "11000010":
            case "11001000":
            case "11001010":
            case "11100000":
            case "11100010":
            case "11101000":
            case "11101010":
                return PresentationTypeType.O253_WALL_NODE;

            //   "x0x0x0x0"
            case "00000000":
            case "00000010":
            case "00001000":
            case "00001010":
            case "00100000":
            case "00100010":
            case "00101000":
            case "00101010":
            case "10000000":
            case "10000010":
            case "10001000":
            case "10001010":
            case "10100000":
            case "10100010":
            case "10101000":
            case "10101010":
                return PresentationTypeType.O255_WALL_NODE;
        }
    
        return PresentationTypeType.NULL;
    }
}