﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Kameosa;
using DungeonGeneration;

public class FixtureInstantiator : MonoBehaviour
{
    public GameObject exit;

    private Dungeon dungeon;

    void Awake()
    {
    }

    public void Instantiate(Dungeon dungeon)
    {
        foreach (Transform childTransform in this.transform)
        {
            GameObject.Destroy(childTransform.gameObject);
        }

        this.dungeon = dungeon;

        InstantiateExit();
    }

    public void InstantiateExit()
    {
        GameObject exit = Instantiate(this.exit, dungeon.Exit.Node.Coordinate, Quaternion.identity).gameObject;

        exit.transform.SetParent(this.transform);
        exit.GetComponent<FixtureController>().node = dungeon.Exit.Node;
    }
}